# Greek Audio Bible EPUB3

## Read the book

This project applies forced alignment to match audio with each verse of the greek new testament, to create a EPUB3 Media Overlay e-book. Reader can click a verse in the book to hear that verse read out.

As the file name suggests, *textus_receptus_1894.epub* contains the [greek TR1894 text](http://textusreceptusbibles.com/Scrivener/40/1). While *tr1894_rylt_interlinear.epub* has [Revised YLT English Bible](http://textusreceptusbibles.com/RYLT/40/1) additionally as interlinear. The audio files packaged in the EPUB is narrated by Theo Karvounakis (modern Greek pronunciation), and made available by Matthias Muller and Glen Scrivener, downloadable from [this site](https://christthetruth.net/2012/06/23/free-greek-audio-bible/). 

To read the e-book, first install an EPUB3 reader that supports the *Media Overlay* format. For desktop (Win/OSX/Linux), I recommend [Thorium](https://www.edrlab.org/software/thorium-reader/). For mobile (ios/android), I recommend [Menestrello](https://www.readbeyond.it/menestrello/)

## Run the code

The code to scrape the text from the website, perform forced alignment between audio and text, and generate the e-book is provided in the *code* folder. I didn't spend extra effort to re-organize the code, therefore you'll see a few absolute paths in the code, which you'll have to modify if you want to run the code.

The following sections talk about how to install the two prerequisites, aeneas and syncabook, and then gives a brief introduction on the code.

## Install aeneas

aeneas helps to perform forced alignment between text and audio. To install, do step 1 and 3 of the **Linux** section of [this site](https://pypi.org/project/aeneas/1.4.0.0/), commands copied as follows

```bash
$ sudo apt install ffmpeg espeak
$ wget https://raw.githubusercontent.com/readbeyond/aeneas/master/install_dependencies.sh
$ sudo bash install_dependencies.sh
$ pip install numpy
$ pip install aeneas
```

## Install syncabook

syncabook generates the EPUB3 Media Overlay file when the text, audio, and synchronization files are organized according to a certain format. To install, following the **Installation** section of [syncabook's page](https://github.com/r4victor/syncabook), commands copied as follows. Note there's no need to install afaligner, I tested and found it does not work well with the greek language.

```
$ git clone https://github.com/r4victor/syncabook/ && cd syncabook
$ python setup.py sdist && pip install dist/syncabook*.tar.gz
$ syncabook -h
```

## The code

**flatten_audio.py** is a simple script to take all audio files and put them in the root of the provided directory. Download the greek audio from [here](https://www.dropbox.com/sh/beoqrdw8zkq1ahr/AABPJTJa5J9RU1y2wyChvPIxa), modify the path to the audio accordingly in **flatten_audio.py** and run.

**generate_book.py** scrapes the text from [this site](http://textusreceptusbibles.com/Scrivener/40/1), puts it in the xhtml format required by EPUB3, calls aeneas to generate the smil synchronization file, creates other files such as the table of contents, and finally calls syncabook to generate the EPUB3 file.

**generate_book_interlinear.py** is modified from **generate_book.py**. It scrapes the RYLT Bible, and adds it to the xhtml file. Note that because of the occasional mismatch of the number of verses in a chapter, the code will not run smoothly, and manual intervention to modify the range of i, j, k may be necessary.

## License

The files in the *code* folder are released under the terms of the GNU Affero General Public License Version 3.

The Bible texts are in public domain, the copyright of the audio files are not clear, but according to the website, it is probably "Freely you have received, freely you shall give". Therefore I assume it is safe to declare the copyright of the EPUB3 books public domain.
