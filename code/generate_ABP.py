import requests
import html
from bs4 import BeautifulSoup
import shutil
import os
import itertools
from aeneas.tools.execute_task import ExecuteTaskCLI
import subprocess
import unicodedata
import sys

IS_GENERATE_XHTML = True
IS_GENERATE_SYNC_XHTML = True
IS_GENERATE_SMIL = False
IS_GENERATE_EPUB_BOOK = True

root_dir = "../"

nChapters = [
    28,
    16,
    24,
    21,
    28,
    16,
    16,
    13,
    6,
    6,
    4,
    4,
    5,
    3,
    6,
    4,
    3,
    1,
    13,
    5,
    5,
    3,
    5,
    1,
    1,
    1,
    22,
]
bookNamesInLink = [
    "matthew",
    "mark",
    "luke",
    "john",
    "acts",
    "romans",
    "1_corinthians",
    "2_corinthians",
    "galatians",
    "ephesians",
    "philippians",
    "colossians",
    "1_thessalonians",
    "2_thessalonians",
    "1_timothy",
    "2_timothy",
    "titus",
    "philemon",
    "hebrews",
    "james",
    "1_peter",
    "2_peter",
    "1_john",
    "2_john",
    "3_john",
    "jude",
    "revelation",
]


# generate chapter names from bookNamesInLink
def capitalizeBookName(name):
    pos = name.find("_")
    if pos != -1:
        name = name.replace("_", " ")
    return name.title()


bookNames = []
for bn in bookNamesInLink:
    bookNames.append(capitalizeBookName(bn))

outputFolder = f"{root_dir}/output/ABP/"
if not os.path.isdir(outputFolder):
    os.mkdir(f"{outputFolder}")
if not os.path.isdir(f"{outputFolder}/no_sync_text"):
    os.mkdir(f"{outputFolder}/no_sync_text")
if not os.path.isdir(f"{outputFolder}/smil"):
    os.mkdir(f"{outputFolder}/smil")
if not os.path.isdir(f"{outputFolder}/sync_text"):
    os.mkdir(f"{outputFolder}/sync_text")
if not os.path.isdir(f"{outputFolder}/aeneas_sync_text"):
    os.mkdir(f"{outputFolder}/aeneas_sync_text")
if not os.path.isdir(f"{outputFolder}/webpage"):
    print("Error: webpage folder is empty. please run scrape_webpages.py first")
    sys.exit()
if not os.path.isdir(f"{outputFolder}/audio"):
    shutil.copytree(
        f"{root_dir}/audio_flatten",
        f"{outputFolder}/audio",
    )
if not os.path.isfile(f"{outputFolder}/style.css"):
    shutil.copy(
        f"{root_dir}/templates/style_interlinear.css",
        f"{outputFolder}/style.css",
    )

# create metadata
metadataContent = """{
  "title": "Apostolic Bible Polyglot New Testment Interlinear",
  "author": "Charles Van der Pool",
  "description": "",
  "narrator": "Theo Karvounakis",
  "contributor": "",
  "transcriber": ""
}"""
metadataPath = outputFolder + "/metadata.json"
metadataFile = open(metadataPath, "w")
metadataFile.write(metadataContent)
metadataFile.close()

# create title
titleContent = """<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="it" xml:lang="it">
  <head>
    <meta http-equiv="default-style" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
    <title>Apostolic Bible Polyglot New Testment Interlinear</title>
  </head>
  <body>
    <section epub:type="bodymatter chapter">
      <h1>Apostolic Bible Polyglot New Testment Interlinear</h1>
      <h2>Charles Van der Pool</h2>
    </section>
  </body>
</html>"""
titlePath = outputFolder + "/no_sync_text/001_title.xhtml"
titleFile = open(titlePath, "w")
titleFile.write(titleContent)
titleFile.close()

# create colophon
colophonContent = """<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="it" xml:lang="it">
  <head>
    <meta http-equiv="default-style" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
    <title>Colophon</title>
  </head>
  <body>
    <section epub:type="bodymatter chapter">
      <h1>Apostolic Bible Polyglot New Testment Interlinear</h1>
      <h2>Charles Van der Pool</h2>
      <p>
        This ebook was produced using audio narrated by Theo Karvounakis (modern Greek pronunciation) from <a href='https://christthetruth.net/2012/06/23/free-greek-audio-bible/'>this site</a>.
        The text is from <a href='https://biblehub.com/interlinear/'>Bible Hub</a>.
        The tools that make this work possible are <a href='https://github.com/readbeyond/aeneas'>aeneas</a> and <a href='https://github.com/r4victor/syncabook'>syncabook</a>.
      </p>
    </section>
  </body>
</html>"""
colophonPath = outputFolder + "/no_sync_text/colophon.xhtml"
colophonFile = open(colophonPath, "w")
colophonFile.write(colophonContent)
colophonFile.close()

# create table of contents
navContent = """<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="en" xml:lang="en">
  <head>
    <meta http-equiv="default-style" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
    <title>Table of Contents</title>
  </head>
  <body>	
    <nav epub:type="toc">
      <h1>Table of Contents</h1>
      <ol>"""

for i in range(0, len(nChapters)):
    bookName = bookNames[i]
    # add chapter to table of contents
    navContent = (
        navContent
        + f"""\n        <li>
          <a href="{40 + i}_{bookName}_01.xhtml">{bookName}</a>
          <ol>"""
    )

    for j in range(1, nChapters[i] + 1):
        # generate file name
        xhtmlName = f"{40 + i}_{bookName}_{j:02}.xhtml"  # 40_Matthew_02.xhtml
        if nChapters[i] > 1:
            mp3Name = (
                f"Koine Greek - {bookName} {j:02}.mp3"  # Koine Greek - Matthew 02.mp3
            )
        else:
            mp3Name = f"Koine Greek - {bookName}.mp3"
        print(mp3Name)

        with open(f"{outputFolder}/webpage/{xhtmlName}", "r") as file:
            htmlText = file.read()

        # change " to ' in Strong's Greek
        pos = -1
        while True:
            pos = htmlText.find('<span class="pos">', pos + 1)
            if pos == -1:
                break
            pos = htmlText.find('title="', pos + 1)
            if pos == -1:
                break
            pos = htmlText.find(":", pos + 1)
            if pos == -1:
                break
            posStart = pos
            pos = htmlText.find('">', pos + 1)
            if pos == -1:
                break
            posEnd = pos
            htmlText = (
                htmlText[:posStart]
                + htmlText[posStart:posEnd].replace('"', "'")
                + htmlText[posEnd:]
            )

        # get content
        soup = BeautifulSoup(htmlText, "html.parser")
        contentTag = soup.body.find(
            "div", attrs={"class": "chap"}
        )  # <div class="chap">

        # add other outer tags
        topHeadingText = soup.body.find(
            "div", attrs={"id": "topheading"}
        ).text  # TODO consider preserve < > with link to previous and next chapter
        topHeadingTag = soup.new_tag("div", id="topheading")
        topHeadingTag.string = topHeadingText[2:-2]
        bodyTag = soup.new_tag("body")
        sectionTag = soup.new_tag("section", attrs={"epub:type": "bodymatter chapter"})
        sectionTag.append(topHeadingTag)
        sectionTag.append(contentTag)
        bodyTag.append(sectionTag)

        headText = """<head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=768, height=1024" />
            <link rel="stylesheet" href="../styles/style.css" type="text/css" />
        </head>"""
        headTag = BeautifulSoup(headText, "html.parser")
        htmlTag = soup.new_tag(
            "html",
            attrs={
                "xmlns": "http://www.w3.org/1999/xhtml",
                "xmlns:epub": "http://www.idpf.org/2007/ops",
                "lang": "ell",
                "xml:lang": "ell",
            },
        )
        htmlTag.append(headTag)
        htmlTag.append(bodyTag)

        # remove translit tag
        translitTags = contentTag.find_all("span", attrs={"class": "translit"})
        for tt in translitTags:
            br = tt.find_next_sibling("br")  # find and remove the br following this tag
            tt.decompose()
            br.decompose()

        # replace [e] with things like N-AMS
        strongsnt2Tags = contentTag.find_all("span", attrs={"class": "strongsnt2"})
        if len(strongsnt2Tags) % 2:
            print(f"Error: odd number of strongsnt2 Tags. {len(strongsnt2Tags)}")
        for k in range(0, len(strongsnt2Tags), 2):
            br = strongsnt2Tags[k + 1].find_previous_sibling("br")
            strongsnt2Tags[k].replace_with(strongsnt2Tags[k + 1].extract())
            br.decompose()

        # complete strong hyper link
        strongTags = contentTag.find_all("span", attrs={"class": "pos"})
        for st in strongTags:
            st.a["href"] = "https://biblehub.com" + st.a["href"]

        # complete grammar hyper link
        grammarTags = contentTag.find_all("span", attrs={"class": "strongsnt2"})
        for gt in grammarTags:
            gt.a["href"] = "https://biblehub.com" + gt.a["href"]

        # Add smil tagging based on verse and punctuation
        smilIndex = 0
        isNextTagNewSmilSection = True
        tablefloatTag = contentTag
        while True:
            tablefloatTag = tablefloatTag.find_next(
                "table", attrs={"class": "tablefloat"}
            )
            if tablefloatTag == None:
                break
            if isNextTagNewSmilSection or (
                tablefloatTag.find("span", attrs={"class": "refmain"}) != None
            ):
                smilIndex = smilIndex + 1
            if tablefloatTag.find("span", attrs={"class": "punct"}):
                isNextTagNewSmilSection = True
            else:
                isNextTagNewSmilSection = False
            spanTags = tablefloatTag.find_all("span")
            for st in spanTags:
                st["id"] = f"f{(smilIndex):04}"
            # update line distance
            tdTag = tablefloatTag.find("td")
            tdTag["height"] = "90"

        # modify cross reference from <a href="../luke/3.htm#23"> to <a href="42_Luke_03.xhtml">
        crossClassTags = contentTag.find_all("div", attrs={"class": "cross"})
        for cct in crossClassTags:
            aTags = cct.find_all("a")
            for at in aTags:
                hrefText = at["href"]
                idx1 = hrefText.find("../")
                idx2 = hrefText.find("/", idx1 + 3)
                idx3 = hrefText.find(".htm")
                bookNameText = hrefText[idx1 + 3 : idx2]
                chapter = int(hrefText[idx2 + 1 : idx3])
                if bookNameText in bookNamesInLink:
                    bookNumber = bookNamesInLink.index(bookNameText)
                    newHrefText = f"{40 + bookNumber}_{bookNames[bookNumber]}_{chapter:02}.xhtml"  # 40_Matthew_02.xhtml
                    at["href"] = newHrefText

        if IS_GENERATE_XHTML:
            with open(f"{outputFolder}/sync_text/{xhtmlName}", "wb") as fOutput:
                fOutput.write(htmlTag.prettify("utf-8"))

        # Create greek file for sync
        if IS_GENERATE_SYNC_XHTML:

            def stripAccents(s):
                return "".join(
                    c
                    for c in unicodedata.normalize("NFD", s)
                    if unicodedata.category(c) != "Mn"
                )

            syncHtmlTag = soup.new_tag(
                "html",
                attrs={
                    "xmlns": "http://www.w3.org/1999/xhtml",
                    "xmlns:epub": "http://www.idpf.org/2007/ops",
                    "lang": "ell",
                    "xml:lang": "ell",
                },
            )
            assembledTag = None
            greekTag = contentTag
            currentId = f"f{(0):04}"
            while True:
                greekTag = greekTag.find_next("span", attrs={"class": "greek"})
                if greekTag == None:
                    if assembledTag != None:
                        syncHtmlTag.append(assembledTag)
                    break
                if greekTag["id"] != currentId:
                    if assembledTag != None:
                        syncHtmlTag.append(assembledTag)
                    currentId = greekTag["id"]
                    assembledTag = soup.new_tag("span", attrs={"id": currentId})
                assembledTag.string = (
                    assembledTag.text + stripAccents(greekTag.text) + " "
                )

            with open(f"{outputFolder}/aeneas_sync_text/{xhtmlName}", "wb") as fOutput:
                fOutput.write(syncHtmlTag.prettify("utf-8"))

        # generate smil file
        if IS_GENERATE_SMIL:
            syncXhtmlPath = outputFolder + "/aeneas_sync_text/" + xhtmlName
            smilPath = outputFolder + "/smil/" + xhtmlName[0:-5] + "smil"
            ExecuteTaskCLI(use_sys=False).run(
                arguments=[
                    None,  # dummy program name argument
                    f"{outputFolder}/audio/{mp3Name}",
                    syncXhtmlPath,
                    f"task_language=ell|os_task_file_format=smil|os_task_file_smil_audio_ref=../audio/{mp3Name}|os_task_file_smil_page_ref=../text/{xhtmlName}|is_text_type=unparsed|is_text_unparsed_id_regex=f[0-9]+|is_text_unparsed_id_sort=numeric",
                    smilPath,
                ]
            )

        # write to content file
        navContent = (
            navContent
            + f'\n            <li><a href="{xhtmlName}">{bookName} {j:02}</a></li>'
        )

    navContent = (
        navContent
        + f"""\n          </ol>
        </li>"""
    )
# save content file
navContent = (
    navContent
    + f"""\n        </ol>
    </nav>
  </body>
</html>"""
)

navPath = outputFolder + "/no_sync_text/nav.xhtml"
navFile = open(navPath, "w")
navFile.write(navContent)
navFile.close()

# run syncabook
if IS_GENERATE_EPUB_BOOK:
    subprocess.run(["syncabook", "create", outputFolder])
