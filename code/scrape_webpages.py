import requests
import html
from bs4 import BeautifulSoup
import shutil
import os
import itertools
from aeneas.tools.execute_task import ExecuteTaskCLI
import subprocess
import unicodedata

nChapters = [28, 16, 24, 21, 28, 16, 16, 13, 6, 6, 4, 4, 5, 3, 6, 4, 3, 1, 13, 5, 5, 3, 5, 1, 1, 1, 22] # TODO make book names same as biblehub link
bookNamesInLink = ["matthew", "mark", "luke", "john", "acts", "romans", "1_corinthians", "2_corinthians", "galatians", "ephesians", "philippians", "colossians", "1_thessalonians", "2_thessalonians", "1_timothy", "2_timothy", "titus", "philemon", "hebrews", "james", "1_peter", "2_peter", "1_john", "2_john", "3_john", "jude", "revelation"]
# generate chapter names from bookNamesInLink
def capitalizeChapterName(name):
    pos = name.find('_')
    if pos != -1:
        name = name.replace('_', ' ')
    return name.title()

chapterNames = []
for bn in bookNamesInLink:
    chapterNames.append(capitalizeChapterName(bn))

outputFolder = '/home/ptyt/Projects_500G/greek_audio_bible_epub3/output/ABP/webpage'
if not os.path.isdir(outputFolder):
    os.mkdir(f'{outputFolder}')

# scrape biblehub
path, dirs, files = next(os.walk(f'{outputFolder}'))
fileCount = len(files)
print(fileCount)
if fileCount != 260:
    for i in range(0, len(nChapters)):
        chapterName = chapterNames[i]
        print(chapterName)
        for j in range(1, nChapters[i] + 1): 
            response = requests.get(f"https://biblehub.com/interlinear/{bookNamesInLink[i]}/{j}.htm")
            htmlText = str(response.content, 'utf-8')
            xhtmlName = f'{40 + i}_{chapterName}_{j:02}.xhtml' #40_Matthew_02.xhtml
            print(xhtmlName)
            with open(f'{outputFolder}/{xhtmlName}', "w") as fOutput:
                fOutput.write(htmlText)

# replace </span></span> wtih </span>
for i in range(0, len(nChapters)):
    chapterName = chapterNames[i]
    print(chapterName)
    for j in range(1, nChapters[i] + 1): 
        isWriteOutput = False
        xhtmlName = f'{40 + i}_{chapterName}_{j:02}.xhtml' #40_Matthew_02.xhtml
        with open(f'{outputFolder}/{xhtmlName}', 'r') as file:
            htmlText = file.read()
            if htmlText.find('</span></span>') != -1:
                print(f'Found </span></span> in {xhtmlName}')
                htmlText = htmlText.replace('</span></span>', '</span>')
                isWriteOutput = True
            for ex in ['?', ',', '.', '!', ';']:
                if htmlText.find(f'</span>{ex}</span>') != -1:
                    print(f'Found </span>{ex}</span> in {xhtmlName}')
                    htmlText = htmlText.replace(f'</span>{ex}</span>', f'{ex}</span>')
                    isWriteOutput = True
            with open(f'{outputFolder}/{xhtmlName}', "w") as fOutput:
                fOutput.write(htmlText)