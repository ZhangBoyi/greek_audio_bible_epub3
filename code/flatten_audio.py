import shutil
import os
import itertools


def move(destination):
    all_files = []
    for root, _dirs, files in itertools.islice(os.walk(destination), 1, None):
        for filename in files:
            all_files.append(os.path.join(root, filename))
    for filename in all_files:
        shutil.move(filename, destination)


def rename(source, destination):
    # Folder source and destination have same number of files. Rename each file in destination with the filenames in source
    files_in_source = sorted(os.listdir(source))
    files_in_destination = sorted(os.listdir(destination))

    # Check if both folders have the same number of files
    if len(files_in_source) != len(files_in_destination):
        print("The folders do not contain the same number of files.")
    else:
        # Rename files in folder A with filenames from folder B
        for file_s, file_d in zip(files_in_source, files_in_destination):
            os.rename(
                os.path.join(destination, file_d), os.path.join(destination, file_s)
            )
        print("Files in destination have been renamed.")


def ratio(source, destination):
    # Folder source and destination have same number of files. Rename each file in destination with the filenames in source
    files_in_source = sorted(os.listdir(source))
    files_in_destination = sorted(os.listdir(destination))

    # Check if both folders have the same number of files
    if len(files_in_source) != len(files_in_destination):
        print("The folders do not contain the same number of files.")
    else:
        # Rename files in folder A with filenames from folder B
        for file_s in files_in_source:
            size_s = os.path.getsize(os.path.join(source, file_s))
            size_d = os.path.getsize(os.path.join(destination, file_s))
            ratio = size_s / size_d
            print(f"{file_s} {ratio}")


if __name__ == "__main__":
    destination = "/home/boyi/git/greek_audio_bible_epub3/audio_flatten"
    move(destination)
    # source = "/home/boyi/git/greek_audio_bible_epub3/audio_flatten_small"
    # rename(source, destination)
    # ratio(source, destination)
