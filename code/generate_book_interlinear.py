import requests
import html
from bs4 import BeautifulSoup
import shutil
import os
import itertools
from aeneas.tools.execute_task import ExecuteTaskCLI
import subprocess

isCreateFolder = False
nChapters = [28, 16, 24, 21, 28, 16, 16, 13, 6, 6, 4, 4, 5, 3, 6, 4, 3, 1, 13, 5, 5, 3, 5, 1, 1, 1, 22]
outputFolder = '/home/ptyt/Projects_500G/greek_audio_bible_epub3/output/tr_Scrivener_interlinear/'
title = 'TR1894 RYLT Interlinear'#'Textus Receptus 1894'

if isCreateFolder:
    if os.path.isdir(outputFolder):
        shutil.rmtree(outputFolder)
    os.mkdir(outputFolder)   
    os.mkdir(f'{outputFolder}/no_sync_text')
    os.mkdir(f'{outputFolder}/smil')
    os.mkdir(f'{outputFolder}/sync_text')
    shutil.copytree('/home/ptyt/Projects_500G/greek_audio_bible_epub3/audio_flatten', f'{outputFolder}/audio')

# create metadata
metadataContent = f"""{{
  "title": "{title}",
  "author": "F. H. A. Scrivener",
  "description": "",
  "narrator": "Theo Karvounakis",
  "contributor": "Boyi",
  "transcriber": ""
}}"""
metadataPath = outputFolder + '/metadata.json'
metadataFile = open(metadataPath, 'w')
metadataFile.write(metadataContent)
metadataFile.close()

# create title
titleContent = f"""<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="it" xml:lang="it">
  <head>
    <meta http-equiv="default-style" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
    <title>T{title}</title>
  </head>
  <body>
    <section epub:type="bodymatter chapter">
      <h1 id="f0001">{title}</h1>
      <h2 id="f0002">by F. H. A. Scrivener</h2>
    </section>
  </body>
</html>"""
titlePath = outputFolder + '/no_sync_text/001_title.xhtml'
titleFile = open(titlePath, 'w')
titleFile.write(titleContent)
titleFile.close()

# create colophon
colophonContent = f"""<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="it" xml:lang="it">
  <head>
    <meta http-equiv="default-style" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
    <title>Colophon</title>
  </head>
  <body>
    <section epub:type="bodymatter chapter">
      <h1>{title}</h1>
      <h2>by F. H. A. Scrivener</h2>
      <p>
        This ebook was produced by Boyi using audio narrated by Theo Karvounakis (modern Greek pronunciation) from <a href='https://christthetruth.net/2012/06/23/free-greek-audio-bible/'>this site</a>.
        The text is from <a href='http://textusreceptusbibles.com/Scrivener'>this site</a>.
        The tools that make this work possible are <a href='https://github.com/readbeyond/aeneas'>aeneas</a> and <a href='https://github.com/r4victor/syncabook'>syncabook</a>.
      </p>
    </section>
  </body>
</html>"""
colophonPath = outputFolder + '/no_sync_text/colophon.xhtml'
colophonFile = open(colophonPath, 'w')
colophonFile.write(colophonContent)
colophonFile.close()

# create table of contents
navContent = """<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="en" xml:lang="en">
  <head>
    <meta http-equiv="default-style" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
    <title>Table of Contents</title>
  </head>
  <body>	
    <nav epub:type="toc">
      <h1>Table of Contents</h1>
      <ol>"""

for i in range(40, 67):
    response = requests.get(f"http://textusreceptusbibles.com/Scrivener/{i}/1")
    # get chapter name
    soup = BeautifulSoup(response.content, 'html.parser')
    chapterNameFull = soup.body.find('h2', attrs={'class':'text-center navref'}).text
    ChapterWordPos = chapterNameFull.find('Chapter')
    chapterName = chapterNameFull[0:ChapterWordPos-1]
    # add chapter to table of contents
    navContent = navContent + f"""\n        <li>
          <a href="{i}_{chapterName}_01.xhtml">{chapterName}</a>
          <ol>"""


    for j in range(1, nChapters[i-40] + 1):
        response = requests.get(f"http://textusreceptusbibles.com/Scrivener/{i}/{j}")
        # get file name
        soup = BeautifulSoup(response.content, 'html.parser')
        chapterNameFull = soup.body.find('h2', attrs={'class':'text-center navref'}).text
        ChapterWordPos = chapterNameFull.find('Chapter')
        chapterName = chapterNameFull[0:ChapterWordPos-1]
        xhtmlName = f'{i}_{chapterName}_{j:02}.xhtml' #40_Matthew_02.xhtml
        if nChapters[i-40] > 1:
            mp3Name = f'Koine Greek - {chapterName} {j:02}.mp3' #Koine Greek - Matthew 02.mp3
        else:
            mp3Name = f'Koine Greek - {chapterName}.mp3'  
        print(mp3Name)

        # create xhtml file
        chapterBody = soup.body.find('table', attrs={'class':'bibletable bibletable18'})
        refs = chapterBody.find_all('td', attrs={'class':'ref'})
        verses = chapterBody.find_all('td', attrs={'class':'greek'})

        # get english text
        englishResponse = requests.get(f"http://textusreceptusbibles.com/RYLT/{i}/{j}") # KJV2016 breaks at Mat9:6, RYLT, Youngs
        englishSoup = BeautifulSoup(englishResponse.content, 'html.parser')
        englishChapterBody = englishSoup.body.find('table', attrs={'class':'bibletable bibletable18'})
        englishVerses = englishChapterBody.find_all('td')


        xhtmlContent = f"""<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="ell" xml:lang="ell">
 <head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=768,height=1024"/>
  <link rel="stylesheet" href="../styles/style.css" type="text/css"/>
  <title>Sonnet I</title>
 </head>
 <body>
  <section epub:type="bodymatter chapter">
   <h1>{chapterName} {j:02}</h1>
   <p>"""
        for k in range(0, len(refs)):
            xhtmlContent = xhtmlContent + f'\n    <span id="f{(k+1):03}">{refs[k].get_text()}\t{verses[k].get_text()}<br/>\t{englishVerses[2*k+1].get_text()}<br/></span><br/>'
        xhtmlContent = xhtmlContent + f"""\n   </p>
  </section>
 </body>
</html>"""
        # save xhtml file
        xhtmlPath = outputFolder + '/sync_text/' + xhtmlName
        xhtmlFile = open(xhtmlPath, 'w')
        xhtmlFile.write(xhtmlContent)
        xhtmlFile.close()

        # # generate smil file
        # smilPath = outputFolder + '/smil/' + xhtmlName[0:-5] + 'smil'
        # ExecuteTaskCLI(use_sys=False).run(arguments=[
        #     None, # dummy program name argument
        #     f'{outputFolder}/audio/{mp3Name}',
        #     xhtmlPath,
        #     f"task_language=ell|os_task_file_format=smil|os_task_file_smil_audio_ref=../audio/{mp3Name}|os_task_file_smil_page_ref=../text/{xhtmlName}|is_text_type=unparsed|is_text_unparsed_id_regex=f[0-9]+|is_text_unparsed_id_sort=numeric",
        #     smilPath
        # ])

        # write to content file
        navContent = navContent + f'\n            <li><a href="{xhtmlName}">{chapterName} {j:02}</a></li>'

    navContent = navContent + f"""\n          </ol>
        </li>"""    
# save content file
navContent = navContent + f"""\n        </ol>
    </nav>
  </body>
</html>"""

navPath = outputFolder + '/no_sync_text/nav.xhtml'
navFile = open(navPath, 'w')
navFile.write(navContent)
navFile.close()

# NOTE change css located at /home/ptyt/.local/lib/python3.8/site-packages/syncabook/templates/style.css

# run syncabook
subprocess.run(['syncabook', 'create', outputFolder])
